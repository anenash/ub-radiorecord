import QtQuick 2.7
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0
import QtMultimedia 5.6

import "../Utils.js" as Utils

Rectangle {
    property string stationId: "rr"
    property string logo: "../../assets/logo.svg"
    property string stationName: "Radio Record"
    property string trackSource: ""
    property string trackTitle: ""
    property int streamId
    property alias player: radioPlayer
    property bool refreshTimer: true

    property variant streamsList: ({})

    ListModel {
        id: streamsModel
    }

    onLogoChanged: {
        console.log("New logo:", logo)
        currentRadioLogo.source = logo
    }

    onStationNameChanged: {
        currentStationName.text = stationName
        console.log("Stream id", streamId)
        if(refreshTimer) {
            bitrateSelector.currentIndex = 1 //streamId
        }
    }

    onStreamsListChanged: {
        streamsModel.clear()
        for(var i in streamsList) {
            streamsModel.append(streamsList[i])
        }
    }

    onTrackSourceChanged: {
        radioPlayer.stop()
        radioPlayer.source = trackSource
        radioPlayer.play()
    }

    onTrackTitleChanged: {
        currentTrack.text = trackTitle
    }

    onRefreshTimerChanged: {
        if (!refreshTimer) {
            refreshData.stop()
        }
    }

    Timer {
        id: refreshData

        interval: 2500
        repeat: true
        running: refreshTimer && radioPlayer.playbackState === MediaPlayer.PlayingState

        onTriggered: {
            Utils.sendHttpRequest("GET", Utils.tracksUrl, updateTrack)
        }

    }

    function updateTrack(data) {
        if(data === "error") {
            console.debug("getStations: Source does not found.")
            return
        }
        var json = JSON.parse(data);
        if(json) {
            for (var i in json.result) {
                if (json.result[i].id === streamId) {
                    var item = json.result[i].track
                    if (item.image600) {
                        currentRadioLogo.source = item.image600
                    } else {
                        currentRadioLogo.source = logo
                    }

                    if (item.artist && item.song) {
                        currentTrack.text = item.artist + " - " + item.song
                    } else {
                        currentTrack.text = ""
                    }
                }
            }
        }
    }

    signal isPlaying(bool value);
    signal nextSong();

    id: playerControl
    height: units.gu(18)
    width: parent.width
    color: theme.palette.normal.base
//    opacity: 0.9
    Divider { anchors.bottom: parent.top }

    ShapedImage {
        id: currentRadioLogo
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: units.gu(1)
        width: units.gu(17)
        height: units.gu(17)
        fadeDuration: 2000
        source: logo
    }
    Label {
        id: currentStationName
        anchors.left: currentRadioLogo.right
        anchors.leftMargin: units.gu(2)
        anchors.top: currentRadioLogo.top
        textSize: Label.Large
        color: theme.palette.normal.baseText
    }
    Label {
        id: currentTrack
        anchors.left: currentRadioLogo.right
        anchors.leftMargin: units.gu(2)
        anchors.top: currentStationName.bottom
        anchors.right: parent.right
        textSize: Label.Small
        wrapMode: Text.WordWrap
        color: theme.palette.normal.baseText
    }

    Icon {
        id: playButton

        visible: parent.enabled
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: units.gu(2)
        name: radioPlayer.playbackState === MediaPlayer.PlayingState?"media-playback-pause":"media-playback-start"
        color: parent.pressed ? theme.palette.normal.focus : theme.palette.normal.foregroundText

        width: units.gu(8)
        height: units.gu(8)

        MouseArea {
            anchors.fill: parent

            onClicked: {
                if (radioPlayer.playbackState === MediaPlayer.PlayingState) {
                    radioPlayer.stop()
                } else {
                    radioPlayer.play()
                }
            }
        }
    }

    ComboBox {
        id: bitrateSelector

        visible: parent.enabled && refreshTimer
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: units.gu(1)
        width: units.gu(15)
        model: streamsModel
        textRole: "text"
        displayText: currentText
//        currentIndex: 1
        onCurrentIndexChanged: {
            console.log(radioPlayer.source)
            radioPlayer.stop()
            radioPlayer.source = streamsModel.get(currentIndex).url
            console.log(radioPlayer.source)
            radioPlayer.play()
        }
    }

    Audio {
        id: radioPlayer
        source: trackSource
        autoLoad: false
        onError: {
            console.log("Error happened", radioPlayer.errorString)
        }
        onPlaying: {
            isPlaying(true)
//                playButton.text = "Stop"
//            playButton.icon.source = "image://theme/icon-l-pause"
//            playRadio = true
//            coberActionButton.iconSource = getIcon(true)
        }
        onStopped: {
            isPlaying(false)
//                playButton.text = "Play"
//            playButton.icon.source = "image://theme/icon-l-play"
//            playRadio = false
//            coberActionButton.iconSource = getIcon(false)
        }

        onStatusChanged: {
            console.log("status changed", status)
            switch (status) {
            case Audio.EndOfMedia:
                nextSong()
                break
            case Audio.Stalled:
                console.log("status Audio.Stalled")
                if (radioPlayer.playbackState !== Audio.StoppedState && app.player.playbackState !== Audio.PausedState) {
                    console.log("Pause player")
                    radioPlayer.pause()
                }
                break
            case Audio.Buffered:
                console.log("status Audio.Buffered")
                if (radioPlayer.playbackState !== Audio.StoppedState && app.player.playbackState === Audio.PausedState) {
                    console.log("start player")
                    radioPlayer.play()
                }
                break
            }
        }
    }
}

