import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3

import "../Components"

ListItem {
    height: units.gu(12)
    property string logo: "../../assets/logo.svg"
    property string stationTitle: ""
    ShapedImage {
        id: radioLogo
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: units.gu(1)
        width: units.gu(10)
        height: units.gu(10)
        fadeDuration: 2000
        source: logo //icon_fill_white
        onStatusChanged: {
            if(status === Image.Error) {
                source = "../../assets/logo.svg"
            }
        }
    }
    Label {
        anchors.left: radioLogo.right
        anchors.leftMargin: units.gu(2)
        anchors.verticalCenter: radioLogo.verticalCenter
        text: stationTitle //title
        font.pointSize: 42
    }

    onClicked: {
        if (radioView.currentIndex !== index) {
            var streams = ([])
            if(stream_64) {
                streams.push( {'text': "64 kbps", 'url': stream_64} )
            }
            if(stream_128) {
                streams.push( {'text': "128 kbps", 'url': stream_128} )
            }
            if(stream_320) {
                streams.push( {'text': "320 kbps", 'url': stream_320} )
            }

            playerPanel.open()
            playerControl.refreshTimer = true
            playerControl.streamsList = streams
            playerControl.streamId = id
            console.log(playerControl.streamId)
            playerControl.stationId = prefix
            playerControl.logo = icon_fill_white
            playerControl.stationName = title
            playerControl.visible = true
            playerControl.enabled = true
            radioView.currentIndex = index
        }

    }
}
