import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3

import "../Components"

ListItem {

    property string logo: "../../assets/logo.svg"
    property string title: ""
    property string track: ""

    height: units.gu(13)
    ShapedImage {
        id: iconButton
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: units.gu(1)
        z: 3
        height: parent.height
        width: height
        fillMode: Image.PreserveAspectFit
        source: logo //image600
        onStatusChanged: {
            if(status === Image.Error) {
                source = "../../assets/logo.svg"
            }
        }
    }

    Label {
        anchors.left: iconButton.right
        anchors.leftMargin: units.gu(1)
        anchors.right: parent.right
        anchors.rightMargin: units.gu(1)
        anchors.top: parent.top
        anchors.topMargin: units.gu(1)
        text: title //artist
        font.pointSize: 32
        wrapMode: Text.WordWrap
    }
    Label {
        anchors.left: iconButton.right
        anchors.leftMargin: units.gu(1)
        anchors.right: parent.right
        anchors.rightMargin: units.gu(1)
        anchors.bottom: parent.bottom
        anchors.bottomMargin: units.gu(1)
        font.pointSize: 30
        text: "Track: " + track //song
        wrapMode: Text.WordWrap
    }
    onClicked: {
        playerPanel.open()
        playerControl.refreshTimer = false
        playerControl.logo = image600
        playerControl.stationName = artist
        playerControl.trackTitle = song
        playerControl.trackSource = link
        playerControl.visible = true
        playerControl.enabled = true
    }
}
