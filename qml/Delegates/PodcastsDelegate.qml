import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3

import "../Components"

ListItem {
    height: units.gu(12)
    property string logo: "../../assets/logo.svg"
    property string podcastTitle: ""
    property var    player

    ShapedImage {
        id: iconButton
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: units.gu(1)
        z: 3
        width: units.gu(18)
        height: units.gu(10)
        fadeDuration: 2000
        source: logo //cover_horizontal
        fillMode: Image.PreserveAspectFit
        onStatusChanged: {
            if(status === Image.Error) {
                source = "../../assets/logo.svg"
            }
        }
    }

    Label {
        anchors.left: iconButton.right
        anchors.leftMargin: units.gu(2)
        anchors.right: parent.right
        anchors.rightMargin: units.gu(2)
        anchors.verticalCenter: iconButton.verticalCenter
        text: podcastTitle //name
        font.pixelSize: 42
        wrapMode: Text.WordWrap
    }

    onClicked: {
        pageStack.push(Qt.resolvedUrl("../PodcastPage.qml"), { 'id': id, 'player': player })
    }
}
