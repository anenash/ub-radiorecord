import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Ubuntu.Components.Themes 1.3

import "Utils.js" as Utils
import "Delegates"

Page {
    anchors.fill: parent

    Component.onCompleted: {
        Utils.sendHttpRequest("GET", Utils.podcastsUrl, getPodcasts)
    }

    function getPodcasts(data) {
        if(data === "error") {
            console.debug("getPodcasts: Source does not found.")
            return;
        }
        var json = JSON.parse(data)
        console.log(JSON.stringify(json))
        for (var i in json.result) {
            pocastsList.append(json.result[i])
        }
    }

    ListModel {
        id: pocastsList
    }

    UbuntuListView {
        id: podcastsView

        anchors.fill: parent
        model: pocastsList
        delegate: PodcastsDelegate {
            logo: cover_horizontal
            podcastTitle: name
        }
        clip: true
        currentIndex: -1
        highlight: Rectangle {
            width: parent.width
            height: units.gu(12)
            color: theme.palette.focused.base
            opacity: 0.3
        }
    }
}
