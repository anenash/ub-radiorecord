import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Components.Themes 1.3

import "Utils.js" as Utils
import "Delegates"

Page {
    id: podcastPage

    property string id

    QtObject {
        id: internal

        property string coverLink: "RadioRecord.png"
        property string pageHeader: qsTr("Podcast")
    }

    Component.onCompleted: {
        var url = Utils.pocaststracsUrl + id
        Utils.sendHttpRequest("GET", url, getPodcastTracks)
    }

    function getPodcastTracks(data) {
        if(data === "error") {
            var url = Utils.podcaststracsUrl + id
            Utils.sendHttpRequest("GET", url, getPodcastTracks)
            return;
        }
        var json = JSON.parse(data)
        console.log("Podcast data\n", JSON.stringify(json))
        for (var i in json.result.tracks) {
            podcastsModel.append(json.result.tracks[i])
        }
    }

    ListModel {
        id: podcastsModel
    }

    UbuntuListView {
        id: podcastView

        anchors.fill: parent
        model: podcastsModel
        delegate: PodcastDelegate {
            logo: image600
            title: artist
            track: song
        }

        clip: true
        currentIndex: -1
        highlight: Rectangle {
            width: parent.width
            height: units.gu(12)
            color: theme.palette.focused.base
            opacity: 0.3
        }
    }
}
