import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Components.Themes 1.3

import "Utils.js" as Utils
import "Delegates"

Page {
    id: rootPage

    anchors.fill: parent

    ListModel {
        id: stationsList
    }

    Component.onCompleted: {
        Utils.sendHttpRequest("GET", Utils.stationsUrl, getStations)
    }

    function getStations(data) {
        if(data === "error") {
            console.debug("getStations: Source does not found.")
            return;
        }
        var json = JSON.parse(data)
        console.log("\nStations\n", JSON.stringify(json))
        if(json) {
            for(var i in json.result.stations) {
                stationsList.append(json.result.stations[i])
            }
        }
    }

    UbuntuListView {
        id: radioView

        anchors.fill: parent
        model: stationsList
        delegate: StationDelegate {
            logo: icon_fill_white
            stationTitle: title
        }

        clip: true
        currentIndex: -1
        highlight: Rectangle {
            width: parent.width
            height: units.gu(12)
            color: theme.palette.focused.base
            opacity: 0.3
        }
    }
}
