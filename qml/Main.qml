/*
 * Copyright (C) 2020  anenash
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * RadioRecordApp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Components.Themes 1.3

import "Utils.js" as Utils
import "Delegates"
import "Components"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'radiorecordapp.anenash'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Tabs {
        id: tabs

        anchors.fill: parent

        selectedTabIndex: 0

        Tab {
            id: radioTab

            title: i18n.tr('Radio Record')

            anchors.fill: parent

            page: Loader {
                anchors.fill: parent
                source: Qt.resolvedUrl("StationsList.qml")
            }
        }

        Tab {
            id: podcastsTab

            title: i18n.tr('Podcasts')

            anchors.fill: parent

            page: PageStack {
                id: pageStack
                anchors.fill: parent

                Component.onCompleted: {
                    push(Qt.resolvedUrl("PodcastsList.qml"))
                }
            }
        }
    }
    
    Panel {
        id: playerPanel
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: units.gu(18)
        animate: true
        PlayerItem {
            id: playerControl

            height: units.gu(18)
            width: parent.width
            anchors.bottom: parent.bottom
            enabled: false
            visible: false
            logo: "../assets/logo.svg"
            stationName: "Radio Record"
        }
    }
}
